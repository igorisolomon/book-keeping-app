export interface User{
  id: number;
  first_name: string;
  last_name: string;
  phone: string;
}

export interface Business{
  id: number;
  business_name: string;
  business_logo: string;
  business_country: string;
  business_state: string;
  business_lga: string;
  business_type: string;
  business_website: string;
}

export interface BusinessType{
  id: number;
  business_type: string;
}

export interface CountryData{
  state: string;
  alias: string;
  lgas: string[];
}

export interface CashflowList{
  count: number;
  next: string;
  previous: string,
  results: {
    id: number,
    description: string;
    date: Date;
    quantity: number;
    price: number;
    cashflow_type: string;
    sales_type: string;
    expense_category: string;
    expense_type: string;
    expense_other: string;
    business: number;
  }[]
}

export interface Cashflow{
  id: number,
  description: string;
  date: Date;
  quantity: number;
  price: number;
  cashflow_type: string;
  sales_type: string;
  expense_category: string;
  expense_type: string;
  expense_other: string;
  business: number;
}

export interface DataSummaryInterface{
  income_sum: {
      sum_income: number;
  };
  expenses_sum: {
      sum_expenses: number
  };
  income_month_list: [
      {
          month: Date,
          sum_month: number
      }
  ];
  expense_month_list: [
      {
        month: Date,
        sum_month: number
      }
  ]
}
