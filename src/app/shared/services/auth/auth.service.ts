import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
// import { Storage } from '@ionic/storage';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

// import firebase from "firebase"

import { environment } from "src/environments/environment";

import { Token } from '../../interfaces/token';
import { DataService } from '../data.service';
import { NavController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  token = new BehaviorSubject<Token>(null);
  accessToken = new BehaviorSubject<string>(null);
  isLoggedIn = new BehaviorSubject<boolean>(false);

  constructor(
    private http: HttpClient,
    private navctrl: NavController,
    // private localStorage: Storage,
    private dataService: DataService
  ) { }

  async checkAuthenticationState() {
    // const token = await localStorage.getItem('shirobookAccessToken');
    // const parsedToken = JSON.parse(token);

    const token = await Storage.get({ key: 'shirobookAccessToken' })
    const parsedToken = JSON.parse(token.value);
    

    if (parsedToken) {
      this.isLoggedIn.next(true);
      this.accessToken.next(parsedToken.access_token);

      this.navctrl.navigateRoot('/')
    }
    else{
      this.navctrl.navigateRoot('/start');
    }
  }

  setToken(token: string){
    this.isLoggedIn.next(true);
    this.accessToken.next(token);

    this.storeToken(token);
  }

  private async storeToken(token: string){
    const accessToken = JSON.stringify({access_token: token});

    // await localStorage.setItem('shirobookAccessToken',accessToken)

    await Storage.set({
      key: 'shirobookAccessToken',
      value: accessToken
    });

    // this.navctrl.navigateRoot(['/tabs/home']);
  }


  /**
   * Signup
   */
  signup(payload) {
    return this.http.post<Token>(
      `${environment.apiUrl}user/create/`,
      payload
    );
  }

  // signup(payload) {
  //   firebase.auth.phon
  // }

  /**
   * Login
   */
  login(payload) {
    return this.http.post<Token>(
      `${environment.apiUrl}user/token/`,
      payload
    );
  }

  async logout(){
    await this.dataService.clear_data()

    // set variable to null
    this.isLoggedIn.next(false);
    this.accessToken.next(null);

    // localStorage.removeItem('shirobookAccessToken');

    await Storage.clear();


    this.navctrl.navigateRoot(['/start']);
  }
}
