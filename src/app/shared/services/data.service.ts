import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

import { environment } from "src/environments/environment";

import { User, Business, BusinessType, CountryData, Cashflow, DataSummaryInterface, CashflowList } from '../interfaces/data';
import { AuthService } from '../services/auth/auth.service';
import { ModalController } from '@ionic/angular';
import { WelcomeBusinessComponent } from 'src/app/components/modals/welcome-business/welcome-business.component';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  user = new BehaviorSubject<User>(null)
  business = new BehaviorSubject<Business>(null)
  business_type = new BehaviorSubject<BusinessType[]>(null)
  country_data = new BehaviorSubject<CountryData[]>(null)
  cashflow = new BehaviorSubject<CashflowList>(null)
  income = new BehaviorSubject<Cashflow[]>(null)
  expenses = new BehaviorSubject<Cashflow[]>(null)
  dataSummary = new BehaviorSubject<DataSummaryInterface>(null)
  expense_cat = new BehaviorSubject<any>(null)

  cashflow_types = [
    {name:'Expense'},
    {name:'Income'}
  ];

  expenses_types = [
    {name: 'Service'},
    {name: 'Product'}
  ];

  sales_types = [
    {name: 'Bank transfer'},
    {name: 'Cash'},
    {name: 'Credit'}
  ];

  constructor(
    private http: HttpClient,
    private router: Router,
    private modalCtrl: ModalController
  ) {}

  clear_data(){
    this.user.next(null);
    this.business.next(null);
    this.cashflow.next(null);
    this.income.next(null);
    this.expenses.next(null);
    this.dataSummary.next(null);
  }

  /**
   * DATA
   */
  getData(){
    return this.http.get<CountryData[]>(
      'assets/data/country.json'
    );
  }

  getDataSummary(id){
    return this.http.get<DataSummaryInterface>(
      `${environment.apiUrl}cashflow/sum/${id}`
    );
  }

  getExpenseCategory(){
    return this.http.get<any>(
      `${environment.apiUrl}expense_category/`
    )
  }


  /**
   * USER
   * *************************************************************** 
   */
  /** Fetch user */
  fetchUser(){
    return this.http.get<User>(
      `${environment.apiUrl}user/me/`
    );
  }

  /** Update user */
  updateUser(payload: User){
    return this.http.patch<User>(
      `${environment.apiUrl}user/me/`,
      payload
    );
  }

  /**
   * *************************************************************** 
   */
  /**
   * Fetch business
   */
  fetchBusinessType(){
    return this.http.get<BusinessType[]>(
      `${environment.apiUrl}business_type/`
    );
  }

  fetchBusiness(id:number){
    return this.http.get<Business>(
      `${environment.apiUrl}business/${id}/`
    )
  }

  createBusiness(payload){
    return this.http.post<Business>(
      `${environment.apiUrl}business/`,
      payload
    )
  }

  updateBusiness(id, payload){
    return this.http.patch<Business>(
      `${environment.apiUrl}business/${id}/`,
      payload
    )
  }

  /**
   * *************************************************************** 
   */
  /**
   * CASHFLOW
   */
  getCashflowList(business_id:number){
    return this.http.get<CashflowList>(
      `${environment.apiUrl}cashflow/business/${business_id}/`
    )
  }

  createCashflow(payload){
    return this.http.post<Cashflow>(
      `${environment.apiUrl}cashflow/business/`,
      payload
    )
  }

  getCashflowDetail(id:number){
    return this.http.get<Cashflow>(
      `${environment.apiUrl}cashflow/${id}/`
    )
  }

  updateCashflow(id:number, payload){
    return this.http.patch<Cashflow>(
      `${environment.apiUrl}cashflow/${id}/`,
      payload
    )
  }

  updateDataServiceCashflow(){
    this.getCashflowList(this.business.value.id).subscribe(
      res=>this.cashflow.next(res)
    )
  }

    
  updateIncomeExpenses(){
    // Fetch income data
    this.getIncomeList(this.business.getValue()?.id).subscribe(
      res=>{
        this.income.next(res);
      }
    )

    this.getExpensesList(this.business.getValue()?.id).subscribe(
      res=>{
        this.expenses.next(res);
      }
    )
  }

  /**
   * *************************************************************** 
   */
  /**
   * INCOME
   */
  getIncomeList(business_id:number){
    return this.http.get<Cashflow[]>(
      `${environment.apiUrl}cashflow/income/business/${business_id}/`
    )
  }


  /**
   * *************************************************************** 
   */
  /**
   * EXPENSES
   */
  getExpensesList(business_id:number){
    return this.http.get<Cashflow[]>(
      `${environment.apiUrl}cashflow/expenses/business/${business_id}/`
    )
  }

  fetchAllData(business_id:number){
    // user
    // business
    // business_type
    // country_data
    // cashflow
    // income
    // expenses
  }

}
