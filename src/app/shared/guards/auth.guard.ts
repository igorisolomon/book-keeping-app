import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable, of } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  
  constructor(
    private authService: AuthService,
    private router: Router,
    private navCtrl: NavController
  ){}

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {

      // this.authService.checkAuthenticationState();

      // if(!this.authService.isLoggedIn.value){
      //   this.navCtrl.navigateRoot('/start');
      //   console.log('hey');
      // }
      // return this.authService.isLoggedIn.value;

      return this.authService.isLoggedIn.pipe(
        take(1),
        tap(isAuthenticated => {
          if (!isAuthenticated) {
            this.navCtrl.navigateRoot('/start');
          }
          return of(isAuthenticated);
        })
      );
  }
}
