import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { DataService } from 'src/app/shared/services/data.service';

@Component({
  selector: 'app-cashflow-input',
  templateUrl: './cashflow-input.component.html',
  styleUrls: ['./cashflow-input.component.scss'],
})
export class CashflowInputComponent implements OnInit {

  cashflowInput: string;
  expenseForm: FormGroup;
  incomeForm: FormGroup;
  exp_cat: any;
  sales_types: any;
  expenses_types: any;
  cashflow_types: any;
  selected_type: string;

  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private toastController: ToastController,
    private dataService: DataService) {
      this.expenseForm = this.fb.group({
        description: new FormControl("", [Validators.required]),
        date: new FormControl("", [Validators.required]),
        quantity: new FormControl("", [Validators.required]),
        price: new FormControl("", [Validators.required]),
        expense_category: new FormControl("", [Validators.required]),
        expense_type: new FormControl("", [Validators.required]),
        expense_other: new FormControl("", [Validators.required]),
      })

      this.incomeForm = this.fb.group({
        description: new FormControl("", [Validators.required]),
        date: new FormControl("", [Validators.required]),
        quantity: new FormControl("", [Validators.required]),
        price: new FormControl("", [Validators.required]),
        sales_type: new FormControl("", [Validators.required]),
      })
    }

  ngOnInit() {
    this.cashflowInput = 'income'

    this.dataService.getExpenseCategory().subscribe(
      res=>{
        this.dataService.expense_cat.next(res);
        this.exp_cat = res;
      }
    )
    // if(this.dataService.expense_cat!=null){
    //   this.exp_cat=this.dataService.expense_cat.value
    //   console.log(`exist1: ${this.exp_cat}`);
      
    // }
    // else{
    //   this.dataService.getExpenseCategory().subscribe(
    //     res=>{
    //       this.dataService.expense_cat.next(res);
    //       this.exp_cat = res;
    //       console.log(`exist2: ${res}`);

    //     }
    //   )
    // }

    this.expenses_types = this.dataService.expenses_types
    this.sales_types = this.dataService.sales_types
    this.cashflow_types = this.dataService.cashflow_types
  }

  async presentToast(status:string, message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: status
    });
    toast.present();
  }

  segmentChanged(e: any) {
    this.cashflowInput = e.detail.value
  }

  typeSelect(e: any){
    this.selected_type = e.detail.value
    console.log(this.selected_type);
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  onExpense(formData){
    const payload = formData.value
    payload['cashflow_type'] = 'Expense'
    payload['business'] = this.dataService.business.value.id

    this.dataService.createCashflow(payload).subscribe(
      res=>{
        this.dataService.updateIncomeExpenses()
        this.presentToast('success', 'Expense added successfully')

        this.dismiss()
      },
    )
  }

  onIncome(formData){
    const payload = formData.value
    payload['cashflow_type'] = 'Income'
    payload['business'] = this.dataService.business.value.id

    this.dataService.createCashflow(payload).subscribe(
      res=>{
        this.dataService.updateIncomeExpenses()
        this.presentToast('success', 'Income added successfully')

        this.dismiss()
      },
    )
  }
}
