import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { BusinessType } from 'src/app/shared/interfaces/data';
import { DataService } from 'src/app/shared/services/data.service';
import { WelcomeAddressComponent } from '../welcome-address/welcome-address.component';

@Component({
  selector: 'app-welcome-business',
  templateUrl: './welcome-business.component.html',
  styleUrls: ['./welcome-business.component.scss'],
})

export class WelcomeBusinessComponent implements OnInit {

  stepOneForm: FormGroup;
  business_types: BusinessType[];

  constructor(
    public modalCtrl: ModalController,
    private fb: FormBuilder,
    private dataService: DataService,
  ) {
    this.stepOneForm = this.fb.group({
      business_name: new FormControl('', [Validators.required]),
      business_description: new FormControl('', [Validators.required]),
      business_type: new FormControl('', [Validators.required]),
      website: new FormControl('')
    })
  }

  ngOnInit() {
    this.dataService.fetchBusinessType().subscribe(
      res=>{
        this.business_types = res;
      }
    )
    if(this.dataService.business){
      this.modalCtrl.dismiss()
    }
  }

  async welcomeAddressModal() {
    const modal = await this.modalCtrl.create({
      component: WelcomeAddressComponent,
      cssClass: 'custom-modal',
      backdropDismiss: false,
    });
    return await modal.present();
  }

  toAddressModal(){

    this.welcomeAddressModal();

    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  stepOne(formData){
    let business_type_data = this.dataService.business_type.value;

    if(business_type_data){
      for (const key in formData.value) {
        business_type_data[key] = formData.value[key];
      }
    }
    else{
      business_type_data = formData.value
    }

    this.dataService.business_type.next(business_type_data);

    this.toAddressModal()
  }

}
