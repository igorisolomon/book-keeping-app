import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { CountryData } from 'src/app/shared/interfaces/data';
import { DataService } from 'src/app/shared/services/data.service';
// import { WelcomeBusinessComponent } from '../welcome-business/welcome-business.component';
import { WelcomeTagComponent } from '../welcome-tag/welcome-tag.component';

@Component({
  selector: 'app-welcome-address',
  templateUrl: './welcome-address.component.html',
  styleUrls: ['./welcome-address.component.scss'],
})
export class WelcomeAddressComponent implements OnInit {

  stepTwoForm: FormGroup;
  countryData: CountryData[];
  state: string;
  lgas:string[];

  constructor(
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private dataService: DataService,
  ) {
    this.stepTwoForm = this.fb.group({
      business_country: new FormControl('', [Validators.required]),
      business_state: new FormControl('', [Validators.required]),
      business_lga: new FormControl('', [Validators.required]),
    })
  }

  ngOnInit() {
    this.dataService.getData().subscribe(
      res=>{
        this.countryData = res;
        this.dataService.country_data.next(res);
      }
    )
  }

  setState(state){
    this.state = state.detail.value
    let state_data = this.countryData.filter(item => item.state.includes(state.detail.value));
    this.lgas = state_data[0].lgas
    
  }

  async welcomeBusinessTag() {
    const modal = await this.modalCtrl.create({
      component: WelcomeTagComponent,
      cssClass: 'custom-modal',
      backdropDismiss: false,
    });

    return await modal.present();
  }

  // async welcomeBusinessModal() {
  //   const modal = await this.modalCtrl.create({
  //     component: WelcomeBusinessComponent,
  //     cssClass: 'custom-modal',
  //     backdropDismiss: false,
  //   });
  //   return await modal.present();
  // }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  toTagModal(){
    this.welcomeBusinessTag()
    this.dismiss()
  }

  // toBusiness(){
  //   this.welcomeBusinessModal()
  //   this.dismiss()
  // }

  stepTwo(formData){
    let business_type_data = this.dataService.business_type.value;
    

    if(business_type_data){
      for (const key in formData.value) {
        business_type_data[key] = formData.value[key];
      }
    }
    else{
      business_type_data = formData.value
    }


    this.dataService.business_type.next(business_type_data);

    this.toTagModal()
  }

}
