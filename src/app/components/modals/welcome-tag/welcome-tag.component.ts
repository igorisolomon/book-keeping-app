import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { DataService } from 'src/app/shared/services/data.service';
import { WelcomeBusinessComponent } from '../welcome-business/welcome-business.component';
// import { WelcomeAddressComponent } from '../welcome-address/welcome-address.component';

@Component({
  selector: 'app-welcome-tag',
  templateUrl: './welcome-tag.component.html',
  styleUrls: ['./welcome-tag.component.scss'],
})
export class WelcomeTagComponent implements OnInit {

  tags = [
    'Transparent',
    'Nurturing',
    'Autonomous',
    'Rewarding',
    'Progressive',
    'Collaborative',
    'Engaging',
    'Empathetic',
    'Innovative',
    'Others'
  ]

  selectedTag = []

  constructor(
    private modalCtrl: ModalController,
    private dataService: DataService,
    private toastController: ToastController,
    private router: Router
  ) { }

  ngOnInit() {}

  async presentToast(status:string, message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: status
    });
    toast.present();
  }

  // async welcomeAddressModal() {
  //   const modal = await this.modalCtrl.create({
  //     component: WelcomeAddressComponent,
  //     cssClass: 'custom-modal',
  //     backdropDismiss: false,
  //   });
  //   return await modal.present();
  // }

  selectTag(tag){

    // if tag in selected tag
    if (this.selectedTag.includes(tag)){
      // remove and set clear
      const index = this.selectedTag.indexOf(tag);
      if (index > -1) {
        this.selectedTag.splice(index, 1);
      }
    }
    
    // else add and set color
    else{
      this.selectedTag.push(tag)
    }
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  // toAddressModal(){
  //   this.welcomeAddressModal()
  //   this.dismiss()
  // }

  save(){
    let business_type_data = this.dataService.business_type.value;

    this.dataService.createBusiness(business_type_data).subscribe(
      res=>{
        this.dataService.business.next(res)
        this.presentToast('success','Business Created Successfully');
        this.dismiss()
      },
      (err) => {
        this.presentToast('danger','Error while creating Business')
      }
    )
      // this.welcomeBusinessModal()
      this.dismiss()
  }


  // async welcomeBusinessModal() {
  //   const modal = await this.modalCtrl.create({
  //     component: WelcomeBusinessComponent,
  //     cssClass: 'custom-modal',
  //     backdropDismiss: false,
  //   });
  //   // modal.onDidDismiss().then(()=>this.refreshData(this.business?.id))
  //   return await modal.present();
  // }
}
