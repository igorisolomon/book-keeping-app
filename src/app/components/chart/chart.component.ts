import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit {

  @Input() income_list:any
  @Input() expenses_list:any
  @Input() month_list

  lineChartData: ChartDataSets[]
  lineChartLabels: Label[]
  lineChartOptions: ChartOptions
  lineChartColors: Color[]
  lineChartLegend: boolean
  lineChartType: string
  lineChartPlugins: any
  



  constructor() {  }

  ngOnInit() {
    const chart = this.callChart()

    this.lineChartData = chart.lineChartData
    this.lineChartLabels = chart.lineChartLabels
    this.lineChartOptions = chart.lineChartOptions
    this.lineChartColors = chart.lineChartColors
    this.lineChartLegend = chart.lineChartLegend
    this.lineChartType = chart.lineChartType
    this.lineChartPlugins = chart.lineChartPlugins
    
  }

  callChart(){
    const lineChartData = [
      { data: this.income_list, label: 'Income'},
      { data: this.expenses_list, label: 'Expense'},
    ];
    const lineChartLabels = this.month_list;
    const lineChartOptions = {
      responsive: true,
      legend:{
        display: false
      },
      scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
      }
    };
    const lineChartColors = [
      {
        borderColor: 'rgba(75,209,111,0.4)',
        backgroundColor: 'rgba(75,209,111,0.2)',
      },
      {
        borderColor: 'rgba(219,15,15,0.4)',
        backgroundColor: 'rgba(219,15,15,0.2)',
      },
    ];
    const lineChartLegend = false;
    const lineChartType = 'line';
    const lineChartPlugins = [  ];

    return {
      lineChartData,
      lineChartLabels,
      lineChartOptions,
      lineChartColors,
      lineChartLegend,
      lineChartType,
      lineChartPlugins
    }
  }



}
