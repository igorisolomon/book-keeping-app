import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Cashflow } from 'src/app/shared/interfaces/data';
import { DataService } from 'src/app/shared/services/data.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {

  @Input() cashflow_id: number;

  edit: boolean;
  editButton: string;
  cashflow: Cashflow;
  detailForm: FormGroup;
  exp_cat: any;
  sales_types: any;
  expenses_types: any;
  cashflow_types: any;
  selected_type: string;

  cashflowInput: string;

  constructor(
    private fb: FormBuilder,
    private dataService: DataService,
    private toastController: ToastController,
  ) {  }

  ngOnInit() {
    this.edit = false;
    this.editButton = 'Edit';

    this.dataService.getExpenseCategory().subscribe(
      res=>{
        this.dataService.expense_cat.next(res);
        this.exp_cat = res;
      }
    )

    this.expenses_types = this.dataService.expenses_types
    this.sales_types = this.dataService.sales_types
    this.cashflow_types = this.dataService.cashflow_types
    
    this.dataService.getCashflowDetail(this.cashflow_id).subscribe(
      res=>{
        this.cashflowInput = res?.cashflow_type;
        this.cashflow=res;
        this.detailForm = this.fb.group({
          description: new FormControl(res?.description, [Validators.required]),
          date: new FormControl(res?.date, [Validators.required]),
          quantity: new FormControl(res?.quantity, [Validators.required]),
          price: new FormControl(res?.price, [Validators.required]),
          sales_type: new FormControl(res?.sales_type, [Validators.required]),
          expense_category: new FormControl(res?.expense_category, [Validators.required]),
          expense_type: new FormControl(res?.expense_type, [Validators.required]),
          expense_other: new FormControl(res?.expense_other, [Validators.required]),
        })

        this.detailForm.disable();
      }
    )
  }

  typeSelect(e: any){
    this.selected_type = e.detail.value
    console.log(this.selected_type);
  }

  async presentToast(status:string, message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: status
    });
    toast.present();
  }


  toEdit(formData){
    if(this.editButton=='Edit'){
      this.editButton='Save'

      this.detailForm.enable();
    }
    else{
      this.editButton='Edit';
      this.detailForm.disable();
    }

    if(this.edit){
      this.dataService.updateCashflow(this.cashflow.id,formData.value).subscribe(
        res=>{
          this.cashflow=res;
          this.dataService.updateDataServiceCashflow()
          this.presentToast('success', 'Updated successfully')
        },
      )
      this.edit=false;
    }

    else{
      this.edit=true;
    }
  }

  onBack(){
    this.editButton='Edit';
    this.edit = false;
    this.detailForm.disable();
  }

}
