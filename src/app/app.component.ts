import { Component } from '@angular/core';

import { MenuController, ModalController, Platform, IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ExportComponent } from './components/export/export.component';
import { createHostListener } from '@angular/compiler/src/core';
import { AuthService } from './shared/services/auth/auth.service';
import { Plugins } from '@capacitor/core';
import { DataService } from './shared/services/data.service';
// import firebase from 'firebase';
import { environment } from 'src/environments/environment';


const { App } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  
  constructor(
    private platform: Platform,
    // private routerOutlet: IonRouterOutlet,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private modalCtrl: ModalController,
    private authService: AuthService,
    private dataService: DataService
  ) {
    // firebase.initializeApp(environment.firebaseConfig)
    this.initializeApp();
    // this.authService.checkAuthenticationState()
    // this.platform.backButton.subscribeWithPriority(-1, () => {
    //   if (!this.routerOutlet.canGoBack()) {
    //     App.exitApp();
    //   }
    // });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.dataService.clear_data()
  }

  async exportComponent() {
    const modal = await this.modalCtrl.create({
      component: ExportComponent,
      cssClass: 'custom-modal',
      swipeToClose: true,
    });
    return await modal.present();
  }

  export(){
    this.exportComponent()
  }

  logout(){
    this.authService.logout()
  }

  exit(){
    App.exitApp();
  }
}
