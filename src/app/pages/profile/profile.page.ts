import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController, NavController, ToastController } from '@ionic/angular';
import { CashflowInputComponent } from 'src/app/components/cashflow-input/cashflow-input.component';
import { WelcomeBusinessComponent } from 'src/app/components/modals/welcome-business/welcome-business.component';
import { BusinessType, CountryData, User } from 'src/app/shared/interfaces/data';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { DataService } from 'src/app/shared/services/data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  profile: string;
  edit: boolean;
  editButton: string;
  user_id: number;
  business_types: BusinessType[];
  state: string;
  lgas: string[];
  countryData: CountryData[];

  personalForm: FormGroup;
  businessForm: FormGroup;

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private authService: AuthService,
    private dataService: DataService,
    private toastController: ToastController) { }

  ngOnInit() {
    this.profile = 'personal';
    this.edit = false;
    this.editButton = 'Edit';

    const user = this.dataService?.user?.value;

    if(user==null){
      this.navCtrl.navigateRoot('/')
    }

    this.user_id = user.id

    this.dataService.fetchBusinessType().subscribe(
      res=>this.business_types=res
    );

    this.dataService.getData().subscribe(
      res=>{
        this.countryData=res;
        this.dataService.country_data.next(res);
      }
    )

    // Fetch user data
    this.dataService.user.subscribe(
      res=>{
        this.personalForm = this.fb.group({
          first_name: new FormControl(res?.first_name, [Validators.required]),
          last_name: new FormControl(res?.last_name, [Validators.required]),
          phone: new FormControl(res?.phone, [Validators.required])
        })
      }
    )

    // fetch business data
    this.dataService.business.subscribe(
      res=>{
        this.businessForm = this.fb.group({
          business_name: new FormControl(res?.business_name, [Validators.required]),
          business_country: new FormControl(res?.business_country, [Validators.required]),
          business_state: new FormControl(res?.business_state, [Validators.required]),
          business_lga: new FormControl(res?.business_lga, [Validators.required]),
          business_type: new FormControl(res?.business_type, [Validators.required]),
          business_website: new FormControl(res?.business_website, [Validators.required]),
        })
      }
    )

    this.personalForm.disable();
    this.businessForm.disable();
  }

  setState(state){
    this.state = state.detail.value
    let state_data = this.countryData.filter(item => item.state.includes(state.detail.value));
    this.lgas = state_data[0].lgas
    
  }

  segmentChanged(e: any) {
    this.profile = e.detail.value
    this.personalForm.disable();
    this.businessForm.disable();
  }

  async presentToast(status:string, message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: status
    });
    toast.present();
  }

  async fetchData(){
    this.dataService.fetchUser().subscribe(
      res=>{
        // this.user = res;
        this.dataService.user.next(res)

        //Fetch business data
        this.dataService.fetchBusiness(res.id).subscribe(
          res=>{
            // this.business = res;
            this.dataService.business.next(res)

            // this.refreshData(this.business?.id)
            // this.dataService.fetchAllData(this.business.id)
          },
          (err)=>{
            this.welcomeBusinessModal()
          }
        )
      },
      // (err)=>{
      //   this.authService.logout()
      // }
    )
  }

  async welcomeBusinessModal() {
    const modal = await this.modalCtrl.create({
      component: WelcomeBusinessComponent,
      cssClass: 'custom-modal',
      backdropDismiss: false,
    });
    // modal.onDidDismiss().then(()=>this.refreshData(this.business?.id))
    return await modal.present();
  }

  async cashflowInputModal() {
    const modal = await this.modalCtrl.create({
      component: CashflowInputComponent,
      cssClass: 'custom-modal',
      swipeToClose: true,
    });
    return await modal.present();
  }

  toEditUser(formData){
    if(this.editButton=='Edit'){
      this.editButton='Save'

      this.personalForm.enable();
    }
    else{
      this.editButton='Edit';
      this.personalForm.disable();
    }

    if(this.edit){
      this.dataService.updateUser(formData.value).subscribe(
        res=>this.dataService.user.next(res)
      )
      this.edit=false;
      this.presentToast('success', 'User profile successfully updated')
      this.personalForm.disable();
    }

    else{
      this.edit=true;
    }
  }

  toEditBusiness(formData){
    if(this.editButton=='Edit'){
      this.editButton='Save'

      this.businessForm.enable();
    }
    else{
      this.editButton='Edit';
      this.businessForm.disable();
    }

    if(this.edit){
      this.dataService.updateBusiness(this.user_id, formData.value).subscribe(
        res=>this.dataService.business.next(res)
      )
      this.edit=false;  
      this.presentToast('success', 'Business profile successfully updated')
    }

    else{
      this.edit=true;
    }
  }

  onBack(){
    this.editButton='Edit';
    this.edit = false;

    this.personalForm.disable();
    this.businessForm.disable();
  }
}
