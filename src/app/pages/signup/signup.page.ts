import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  signupForm: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private navCtrl: NavController,
    private toastController: ToastController
  ) {
    this.signupForm = fb.group({
      first_name: new FormControl('', [Validators.required]),
      last_name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      password2: new FormControl('', [Validators.required]),
    })
  }

  ngOnInit() {
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  async presentToast(status:string, message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: status
    });
    toast.present();
  }

  onSignup(formData:FormGroup){
    console.log(formData);
    
    if(formData.value.password===formData.value.password2){
      this.authService.signup(formData.value).subscribe(
        res=>{
          this.authService.login({
            phone:formData.value.phone,
            password: formData.value.password
          }).subscribe(
            res=>{
              this.authService.setToken(res.token);
              this.navCtrl.navigateRoot('/tabs/cashflow');
              this.presentToast('success','Login Successful');
            },
            (err) => {
              this.presentToast('danger','Invalid phone number or password')
            }
          )
        },
        (err) => {
          this.presentToast('danger', err.error.phone[0])
        }
      )
    }
    else this.presentToast('danger',"Password doesn't match")
  }
}
