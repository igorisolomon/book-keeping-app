import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {

  slideOpts = {
    autoplay: true,
    initialSlide: 0,
    speed: 400,
    zoom: {
      maxRatio: 5
    },
  };

  constructor() { }

  ngOnInit() {
  }

}
