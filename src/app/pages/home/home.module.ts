import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { WelcomeBusinessComponent } from 'src/app/components/modals/welcome-business/welcome-business.component';
import { WelcomeAddressComponent } from 'src/app/components/modals/welcome-address/welcome-address.component';
import { WelcomeTagComponent } from 'src/app/components/modals/welcome-tag/welcome-tag.component';
import { ChartsModule } from 'ng2-charts';
import { CashflowInputComponent } from 'src/app/components/cashflow-input/cashflow-input.component';
import { DetailComponent } from 'src/app/components/detail/detail.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from 'src/app/shared/interceptors/auth-interceptors';
import { ChartComponent } from 'src/app/components/chart/chart.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MDBBootstrapModule.forRoot(),
    NgbProgressbarModule,
    ChartsModule,
    HomePageRoutingModule
  ],
  declarations: [
    HomePage,
    WelcomeBusinessComponent,
    WelcomeAddressComponent,
    WelcomeTagComponent,
    CashflowInputComponent,
    DetailComponent,
    ChartComponent,
  ],
  providers:[
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ]
})
export class HomePageModule {}
