import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavController } from "@ionic/angular";
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { DetailComponent } from 'src/app/components/detail/detail.component';
import { CashflowInputComponent } from 'src/app/components/cashflow-input/cashflow-input.component';
import { WelcomeAddressComponent } from 'src/app/components/modals/welcome-address/welcome-address.component';
import { Business, Cashflow, CashflowList, User } from 'src/app/shared/interfaces/data';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { DataService } from 'src/app/shared/services/data.service';

import { WelcomeBusinessComponent } from "../../components/modals/welcome-business/welcome-business.component";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  user: User;
  business: Business;
  cashflow: any;
  income: number;
  expenses: number;
  month_list;
  income_list;
  expenses_list;

  constructor(
    public modalCtrl: ModalController,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private authService: AuthService,
    private dataService: DataService,
  ) {  }

  ngOnInit() {
    this.fetchData();
    this.dataService.user.subscribe(res=>{this.user=res})
    this.dataService.business.subscribe(res=>{this.business=res})
    this.dataService.dataSummary.subscribe(res=>this.income=res?.income_sum?.sum_income)
    this.dataService.dataSummary.subscribe(res=>this.expenses=res?.expenses_sum?.sum_expenses)
    this.dataService.dataSummary.subscribe(res=>this.expenses=res?.expenses_sum?.sum_expenses)
    this.dataService.dataSummary.subscribe(res=>this.expenses=res?.expenses_sum?.sum_expenses)
    this.dataService.dataSummary.subscribe(res=>this.expenses=res?.expenses_sum?.sum_expenses)
    this.dataService.cashflow.subscribe(res=>this.cashflow=res?.results)
  }

  ionViewDidEnter(){

    this.fetchData();
    // if(this.business?.id){
    //   this.refreshData(this.business?.id)
    // }
    // else{      
    //   this.dataService.fetchBusiness(this.user?.id).subscribe(
    //     res=>{
    //       // this.business = res;
    //       this.dataService.business.next(res)

    //       this.refreshData(this.business?.id)
    //     }
    //   )
    // }
  }

  fetchData(){
    this.dataService.fetchUser().subscribe(
      res=>{
        // this.user = res;
        this.dataService.user.next(res)
        // console.log(this.dataService.user.value);
        


        //Fetch business data
        this.dataService.fetchBusiness(res.id).subscribe(
          res=>{
            // this.business = res;
            this.dataService.business.next(res)

            // console.log(this.dataService.business.value);


            this.refreshData(res.id)
          },
          (err)=>{
            this.welcomeBusinessModal()
          }
        )
      },
      // (err)=>{
      //   this.authService.logout()
      // }
    )
  }

  refreshData(id: number){
    this.dataService.getDataSummary(id).subscribe(
      res=>{
        this.income = res?.income_sum?.sum_income;
        this.expenses = res?.expenses_sum?.sum_expenses;
        this.dataService.dataSummary.next(res)

        let month_income = res?.income_month_list.map(x=>x.month)
        let month_expense = res?.expense_month_list.map(x=>x.month)

        let month = new Set(month_income.concat(month_expense))

        this.month_list = [...month]

        this.income_list = res?.income_month_list.map(x=>x.sum_month)
        this.expenses_list = res?.expense_month_list.map(x=>x.sum_month)

      }
    )

    // Fetch cashflow data
    this.dataService.getCashflowList(id).subscribe(
      res=>{
        // this.cashflow = res.results;
        // console.log(res.results);
        
        this.dataService.cashflow.next(res);
      }
    )
  }

  

  async welcomeBusinessModal() {
    const modal = await this.modalCtrl.create({
      component: WelcomeBusinessComponent,
      cssClass: 'custom-modal',
      backdropDismiss: false,
    });
    // modal.onDidDismiss().then(()=>this.refreshData(this.business?.id))
    return await modal.present();
  }

  async cashflowInputModal() {
    const modal = await this.modalCtrl.create({
      component: CashflowInputComponent,
      cssClass: 'custom-modal',
      swipeToClose: true,
    });
    modal.onDidDismiss().then(()=>this.fetchData())
    return await modal.present();
  }

  async detailComponent(id:number) {
    const modal = await this.modalCtrl.create({
      component: DetailComponent,
      cssClass: 'custom-modal',
      swipeToClose: true,
      componentProps: {
        'cashflow_id': id
      }
    });
    modal.onDidDismiss().then(()=>this.fetchData())
    return await modal.present();
  }

  async loadingContentSpinner() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      // duration: 5000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading',
      // backdropDismiss: true
    });
    await loading.present();

    this.dataService.user.subscribe(
      res=>{
        if(res){
          loading.dismiss()
        }
      }
    )

    const { role, data } = await loading.onDidDismiss();
  }

  toCurrency(currency:number){
    const formatted_currency = currency?.toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    })
    return `₦ ${formatted_currency}`
  }
}
