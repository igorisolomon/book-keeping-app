import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private navctrl: NavController,
    private toastController: ToastController
  ) {
    this.loginForm = fb.group({
      phone: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })
  }

  ngOnInit() {
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  async presentToast(status:string, message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: status
    });
    toast.present();
  }

  onLogin(formData:FormGroup){
    this.authService.login(formData.value).subscribe(
      res=>{
        // Fetch user
        // Fetch business
        this.authService.setToken(res.token);
        this.navctrl.navigateRoot(['/']);
        
        // this.router.navigateByUrl('/');
        // this.router.navigateByUrl('/tabs/cashflow');
        this.presentToast('success','Login Successful');
      },
      (err) => {
        this.presentToast('danger','Invalid phone number or password')
      }
    )
  }

}
