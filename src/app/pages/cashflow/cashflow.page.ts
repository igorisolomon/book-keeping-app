import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { DetailComponent } from 'src/app/components/detail/detail.component';
import { CashflowInputComponent } from 'src/app/components/cashflow-input/cashflow-input.component';
import { Cashflow } from 'src/app/shared/interfaces/data';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { DataService } from 'src/app/shared/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cashflow',
  templateUrl: './cashflow.page.html',
  styleUrls: ['./cashflow.page.scss'],
})
export class CashflowPage implements OnInit {

  cashflow: string;
  income: Cashflow[];
  expenses: Cashflow[];

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private authService: AuthService,
    private dataService: DataService,
  ) { }

  ngOnInit() {
    this.cashflow = 'income';
    this.fetchData()
    this.dataService.income.subscribe(res=>this.income=res)
    this.dataService.expenses.subscribe(res=>this.expenses=res)
  }

  ionViewDidEnter(){
    this.fetchData()
    this.dataService.income.subscribe(res=>this.income=res)
    this.dataService.expenses.subscribe(res=>this.expenses=res)
  }

  segmentChanged(e: any) {
    this.cashflow = e.detail.value
  }

  async fetchData(){
    const business = this.dataService.business.value

    if(business==null){
      this.navCtrl.navigateRoot('/')
    }
  
    // Fetch income data
    this.dataService.getIncomeList(business?.id).subscribe(
      res=>{
        // this.income = res;
        this.dataService.income.next(res);
      }
    )

    // Fetch expense data
    this.dataService.getExpensesList(business?.id).subscribe(
      res=>{
        // this.expenses = res;
        this.dataService.expenses.next(res);
      }
    )
    
  }

  async cashflowInputModal() {
    const modal = await this.modalCtrl.create({
      component: CashflowInputComponent,
      cssClass: 'custom-modal',
      swipeToClose: true,
    });
    return await modal.present();
  }

  async detailComponent(id:number) {
    const modal = await this.modalCtrl.create({
      component: DetailComponent,
      cssClass: 'custom-modal',
      swipeToClose: true,
      componentProps: {
        'cashflow_id': id
      }
    });
    return await modal.present();
  }

}
